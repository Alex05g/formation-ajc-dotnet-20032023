﻿
using tva.api.Calculateurs;
using tva.api.Displayers;

TvaCalculateur calculateur = new ();
TvaDisplayer displayer = new();

List<decimal> tvaList = new List<decimal>();

calculateur.InitialiserListe(tvaList, Console.ReadLine, Console.WriteLine);

displayer.Display(tvaList, Console.WriteLine);

var tvasFiltrees = calculateur.FilterTVA(tvaList);
var tvasFiltrees2 = calculateur.FilterTVA(tvaList, 0.056M);
displayer.Display(tvasFiltrees, Console.WriteLine);

void AfficherEnVert(object message)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(message);
    Console.ForegroundColor = ConsoleColor.White;
}
