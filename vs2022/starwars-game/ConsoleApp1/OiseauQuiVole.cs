﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decouverte_interfaces
{
    internal class OiseauQuiVole : Oiseau, IQuiPeutVoler
    {
        public virtual void Voler()
        {
            Console.WriteLine("Je vole");
        }
    }
}
