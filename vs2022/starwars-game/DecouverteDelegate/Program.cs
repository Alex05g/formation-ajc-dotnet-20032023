﻿

using DecouverteDelegate;


void CalculerSomme(Afficher afficher, params int[] values)
{
    int result = values.Sum();
    #region Ici ca peut changer
    //Console.ForegroundColor = ConsoleColor.Red;
    // Console.WriteLine(result);
    //Console.ForegroundColor = ConsoleColor.White;

    #endregion

    // Quoi : JeVeuxAfficher(resultat)
    // Signature : void Afficher(object unParam);
    afficher(result); // Je ne sais pas ici comment ça va etre affiché, ce n'est pas mon rôle / ma responsabilité
}

void AfficherEnCouleur(object item, ConsoleColor couleur)
{
    Console.ForegroundColor = couleur;
    Console.WriteLine(item);
    Console.ForegroundColor = ConsoleColor.White;
}

void AfficherEnRouge(object item)
{
    AfficherEnCouleur(item, ConsoleColor.Red);
}

void AfficherEnVert(object item)
{
    AfficherEnCouleur(item, ConsoleColor.Green);
}

CalculerSomme(Console.WriteLine, 1, 2, 5, 6, 7, 8, 10, 15);
CalculerSomme(AfficherEnVert, 1, 2, 5, 6, 7, 8, 10, 15);



#region Etude de cas d'un delegue
Afficher afficher = Console.WriteLine;
Afficher autreChose = afficher;

LectureSaisie read = Console.ReadLine;

afficher("coucou");
autreChose("Test");
#endregion

Console.ReadLine();