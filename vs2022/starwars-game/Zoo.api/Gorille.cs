﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo.api
{
    public class Gorille : Animal
    {
        public Gorille(string name, bool hostile) : base(name, hostile)
        {
        }

        public override void Attaquer(Animal animal)
        {
            Console.WriteLine("J'attaque avec mes poings !");
            base.Attaquer(animal);
        }

        public override string ToString()
        {
            return $"{this.Name} - En vie ? {this.EstEnVie}";
        }
    }
}
