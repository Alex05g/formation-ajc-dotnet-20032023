﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Adapters
{
    /// <summary>
    /// Contract à implémenter pour sauvegarder une partie
    /// </summary>
    public interface IGameSauvegarde
    {
        /// <summary>
        /// Enregistre une seule partie (checkpoint)
        /// </summary>
        /// <param name="checkPoint"></param>
        void SaveOne(CheckPoint checkPoint);

        /// <summary>
        /// Tous les checkpoints ....
        /// </summary>
        /// <param name="checkPoints"></param>
        void SaveAll(List<CheckPoint> checkPoints); 
    }
}
