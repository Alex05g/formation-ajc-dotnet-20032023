﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuFeuLesPompiers
{
    internal class Ambulance
    {
        public void Secourir(Personne p)
        {
            Console.WriteLine($"Ambulance : Au secours de {p.Nom}");
        }
    }
}
