﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuFeuLesPompiers
{
    internal class Pompier
    {

        /// <summary>
        /// Commentaire qui explique pourquoi et quand
        /// </summary>
        public event Action<Personne> prevenirSecours;
        public void EteindreFeu(Personne personne)
        {
            Console.WriteLine($"Pompiers : Au secours de {personne.Nom}");
            prevenirSecours?.Invoke(personne);
        }
    }
}
