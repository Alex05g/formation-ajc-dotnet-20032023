﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api.Calculateurs
{
    /// <summary>
    /// Classe qui permet de calculer la TVA
    /// </summary>
    public class TvaCalculateur
    {
        public decimal CalculerTTC(decimal tva)
        {
            return tva * (1 + tva);
        }

        public decimal CalculerTTC(decimal montantHT, FournirTvaCourante getTva)
        {
            return montantHT * (1 + getTva());
        }
        /// <summary>
        /// Dmande la saisie de chaque TVA à insérer dans la liste
        /// </summary>
        /// <param name="tvas">Liste de TVA venant du program</param>
        /// 

        public List<decimal> FiltrerTva(List<decimal> tvas, decimal filtre = 0.055M)
        {
            return tvas.Where(item => item >= filtre).ToList();
        }
        public void InitialiserListe(List<decimal> tvas, RecupererSaisie recupererSaisie, 
            AfficherInformation afficher)
        {
            bool arretSaisie = false;
            do
            {
                afficher("Nouvelle TVA ? (STOP pour arreter");
                var saisieUtilisateur = recupererSaisie();

                arretSaisie = saisieUtilisateur == "STOP";
                if (!arretSaisie)
                {
                    if (decimal.TryParse(saisieUtilisateur, out var tva))
                    {
                        tvas.Add(tva);
                    }
                }
            }
            while (!arretSaisie);
        }
    }
}
