﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api
{
    /// <summary>
    /// Récupère la TVA concernée pour le calcul de TTC
    /// </summary>
    /// <returns></returns>
    public delegate decimal FournirTvaCourante();
    /// <summary>
    /// Affichage de l'info vers la sortie utilisateur
    /// </summary>
    /// <param name="info"></param>
    public delegate void AfficherInformation(object info);

    /// <summary>
    /// Récupère la saisie utilisateur
    /// </summary>
    /// <returns></returns>
    public delegate string RecupererSaisie();

}
