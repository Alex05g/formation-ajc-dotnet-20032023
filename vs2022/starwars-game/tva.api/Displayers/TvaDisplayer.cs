﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tva.api;
using tva.api.Calculateurs;
using tva.api.Displayers;

Console.OutputEncoding = Encoding.UTF8;

namespace tva.api.Displayers
{
    /// <summary>
    /// Permet d'afficher une liste de tva sous un format donnée
    /// </summary>
    public class TvaDisplayer
    {
        #region Public methods
        /// <summary>
        /// Affiche la liste des TVA
        /// </summary>
        /// <param name="tvaList"></param>
        public void Display(List<Decimal> tvaList, AfficherInformation afficher)
        {
            var query = from tva in tvaList
                        select new
                        {
                            TVABrut = tva,
                            TVAEuro = $"{tva} €"
                        };
            foreach (var item in query)
            {
                afficher($"{item.TVABrut} // {item.TVAEuro}");
            }
        }
        #endregion
    }
}
