﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PersonneController : Controller
    {
        public IActionResult List()
        {
            List<Personne> list = new()
            {
                new() {Id = 1, Prenom = "Barnabé"},
                new() {Id = 2, Prenom = "Bernard"},
                new() {Id = 2, Prenom = "Bob"},
                new() {Id = 2, Prenom = "Jacquouille la fripouille"},
                new() {Id = 2, Prenom = "Messire Godefroy de Montmirail"}
            };

            return View("List2", list);
        }
        public IActionResult Edit()
        {
            return View();
        }
    }
}
