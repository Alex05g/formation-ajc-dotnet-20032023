﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.SW_class
{
    public class Enemy : Character
    {
        public event Action<Enemy> mortEnnemi;
        public Enemy(string name, int hp) : base(name, hp)
        {

        }

        public override void Attack(Character character)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{Name} vous attaque");
            base.Attack(character);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public override void Defend(Character character)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{Name} se défend");
            base.Defend(character);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void MortEnnemi()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{this.Name} est mort");
            this.mortEnnemi?.Invoke(this);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
