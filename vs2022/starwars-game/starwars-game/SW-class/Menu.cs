﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using starwars_game;
using static System.Net.Mime.MediaTypeNames;

namespace starwars_game.SW_class
{
    public class Menu
    {
        Afficher afficher = Console.WriteLine;
        #region Constructor
        public Menu()
        {
        }
        #endregion

        #region Properties
        public bool MenuNouvellePartie()
        {
            afficher("\nQuel est votre prénom ?\n");
            string nomUtilisateur = Console.ReadLine();
            afficher("\nQuel est votre date de naissance ?\n");
            string dateNaisUtilisateur = Console.ReadLine();
            DateTime dateNaisConv = DateTime.Parse(dateNaisUtilisateur);
            var comparasionDate = DateTime.Now - dateNaisConv;
            int ageJoueur = (int)(comparasionDate.TotalDays / 365);

            if (ageJoueur < 13)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                afficher("\nVous n'avez pas l'âge requis pour jouer\n");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                return false;
            }
            else
            {
                afficher("\nQuel est votre âge ?\n");
                string verifAgeUser = Console.ReadLine();
                int verifAgeUserConv = int.Parse(verifAgeUser);
                if (verifAgeUserConv != ageJoueur)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    afficher("\nInformation erronée\n");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    return false;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    afficher($"\nBienvenue {nomUtilisateur}\n");
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    return true;
                }
            }
        }

        public Hero MenuAjouterHero()
        {
            afficher("\nQuel est le nom de votre personnage ?\n");
            string heroName = Console.ReadLine();
            Hero hero = new Hero(heroName, 100);
            afficher($"Bienvenue sur Tatooine {hero.Name} !");
            return hero;
        }

        public void MenuChargerPartie()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            afficher("\nFonction pas encore disponible\n");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
        }

        public void MenuOptions()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            afficher("\nFonction pas encore disponible\n");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
        }
        public void GameOverScreen()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            afficher("\nGAME OVER");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
        }

        #endregion
    }
}
