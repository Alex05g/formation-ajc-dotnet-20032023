﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.SW_class
{
    /// <summary>
    /// Type de personnage possible
    /// </summary>
    public enum CharacterType
    {
        Player,
        Enemy
    }
}
