﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.SW_class
{
    public class Character
    {
        #region Fields
        private string name = "";
        private int hp;
        private int positionX;
        private int positionY;

        public string Name { get => name; set => name = value; }
        public int HP { get => hp; set => hp = value; }
        public int PositionX { get => positionX; set => positionX = value; }
        public int PositionY { get => positionY; set => positionY = value; }
        #endregion

        #region Constructor
        public Character(string name, int hp)
        {
            this.Name = name;

            this.HP = hp;
        }
        public Character()
        {

        }
        #endregion

        #region Properties
        public virtual void Attack(Character character)
        {
            Console.WriteLine($"{Name} attaque");
        }

        public virtual void Defend(Character character)
        {
            Console.WriteLine($"{Name} se défend");
        }
        #endregion
    }
}
