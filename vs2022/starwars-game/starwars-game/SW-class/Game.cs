﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace starwars_game.SW_class
{
    public class Game
    {
        public event Action<Game> demarragePartie;
        #region Fields
        private int idGame;
        private DateTime gameStartDate;
        private List<Checkpoint> saves;
        private string playerName;

        public int IdGame { get => idGame; set => idGame = value; }
        public DateTime GameStartDate { get => gameStartDate; set => gameStartDate = value; }
        public List<Checkpoint> Saves { get => saves; set => saves = value; }
        public string PlayerName { get => playerName; set => playerName = value; }
        #endregion

        #region Constructor
        public Game(int id, List<Checkpoint> saves, string playerName)
        {
            this.IdGame = id;
            this.GameStartDate = DateTime.Now;
            this.Saves = saves;
            this.PlayerName = playerName;
        }
        #endregion

        #region Properties
        public void NewCheckpoint(Checkpoint checkpoint)
        {
            Saves.Add(checkpoint);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Partie sauvegardée");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void DemarragePartie()
        {
            Console.WriteLine($"La partie commence");
            this.demarragePartie?.Invoke(this);
        }
        public void GameOver(Hero hero)
        {
            hero.MortHero();
        }
        public string RetournerInfosGame()
        {
            string infos = $"\n{this.IdGame.ToString()} - Joueur : {this.PlayerName} - Nombre de sauvegardes : {this.Saves.Count()} - {this.GameStartDate}\n";
            return infos;
        }
        /*public void SauvegarderFormatTexte(Checkpoint checkpoint, string chemin)
        {
            try
            {
                File.WriteAllText(chemin, checkpoint.RetournerInfosCheckpoint());
            }
            catch (PathTooLongException ex)
            {

            }
            catch (IOException ex)
            {

            }
        }
        public void SauvegarderFormatXml(Checkpoint checkpoint, string chemin)
        {


            var type = typeof(Checkpoint);

            XmlSerializer serializer = new XmlSerializer(typeof(Checkpoint));

            using var streamF = new FileStream(chemin, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            using var stream = new StreamWriter(chemin);
            try
            {
                serializer.Serialize(stream, checkpoint);
            }
            finally
            {
                stream.Close();
                streamF.Close();
            }

        }
        public void SauvegarderFormatJson(Checkpoint checkpoint, string chemin)
        {
            string jsonCheckpoint = JsonConvert.SerializeObject(checkpoint, Newtonsoft.Json.Formatting.Indented);

            try
            {
                File.WriteAllText(chemin, jsonCheckpoint);
            }
            catch (PathTooLongException ex)
            {

            }
            catch (IOException ex)
            {

            }
        }*/

        #endregion
    }
}
