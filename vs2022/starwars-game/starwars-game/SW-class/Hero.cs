﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.SW_class
{
    public class Hero : Character
    {
        public event Action<Hero> mortDuHero;

        public Hero(string name, int hp) : base(name, hp)
        {

        }
        public Hero() { }

        public override void Attack(Character character)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{Name} vous attaque");
            base.Attack(character);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public override void Defend(Character character)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{Name} se défend");
            base.Defend(character);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void MortHero()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\n{this.Name} est mort\n");
            this.mortDuHero?.Invoke(this);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
