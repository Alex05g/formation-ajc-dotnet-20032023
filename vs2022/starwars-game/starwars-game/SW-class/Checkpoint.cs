﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace starwars_game.SW_class
{
    public class Checkpoint
    {
        #region Fields
        public Player player;
        private int checkpointId;
        private string playerName;
        private DateTime currentCheckpointDate;
        private int currentPlayerHp;
        private int currentPositionPlayerX;
        private int currentPositionPlayerY;

        public int CheckpointId { get => checkpointId; set => checkpointId = value; }
        public string PlayerName { get => playerName; set => playerName = value; }
        public DateTime CurrentCheckpointDate { get => currentCheckpointDate; set => currentCheckpointDate = value; }
        public int CurrentPlayerHp { get => currentPlayerHp; set => currentPlayerHp = value; }
        public int CurrentPositionPlayerX { get => currentPositionPlayerX; set => currentPositionPlayerX = value; }
        public int CurrentPositionPlayerY { get => currentPositionPlayerY; set => currentPositionPlayerY = value; }
        #endregion

        #region Constructor
        public Checkpoint(int checkpoint_id, Hero hero, int currentPlayerHp, int currentPositionPlayerX, int currentPositionPlayerY)
        {
            this.CheckpointId = checkpoint_id;
            this.CurrentCheckpointDate = DateTime.Now;
            this.CurrentPlayerHp = hero.HP;
            this.CurrentPositionPlayerX = hero.PositionX;
            this.CurrentPositionPlayerY = hero.PositionY;
            this.PlayerName = hero.Name;
        }

        public Checkpoint()
        {

        }

        public string RetournerInfosCheckpoint()
        {
            string infos = $"\n{this.CheckpointId.ToString()} - Joueur : {this.PlayerName}, coordonnées : {this.currentPositionPlayerX}, {this.CurrentPositionPlayerY}, {this.currentPlayerHp.ToString()} PV\n";
            return infos ;
        }

        

        #endregion
    }


}
