﻿using starwars_game;
using starwars_game.SW_class;
Console.OutputEncoding = System.Text.Encoding.UTF8;

List<double> l_tva = new List<double>();
var monTitre = "a starwars game".ToUpper();
Afficher afficher = Console.WriteLine;
Menu menu = new Menu();

#region Entrainement 009
List<Checkpoint> list_checkpoints = new List<Checkpoint>();
Hero hero = new Hero("Jedi", 100);
hero.PositionX = 1;
hero.PositionY = 1;
Checkpoint checkpoint1 = new Checkpoint(01, hero, hero.HP, hero.PositionX, hero.PositionY);
hero.PositionX = 2;
hero.PositionY = 3;
hero.HP = 90;
Checkpoint checkpoint2 = new Checkpoint(02, hero, hero.HP, hero.PositionX, hero.PositionY);
hero.PositionX = 6;
hero.PositionY = 9;
hero.HP = 75;
Checkpoint checkpoint3 = new Checkpoint(03, hero, hero.HP, hero.PositionX, hero.PositionY);
hero.PositionX = 3;
hero.PositionY = 14;
hero.HP = 95;
Checkpoint checkpoint4 = new Checkpoint(04, hero, hero.HP, hero.PositionX, hero.PositionY);
hero.PositionX = 15;
hero.PositionY = 9;
hero.HP = 45;
Checkpoint checkpoint5 = new Checkpoint(05, hero, hero.HP, hero.PositionX, hero.PositionY);
var path_file_text = Path.Combine(Environment.CurrentDirectory, "checkpoints_text.txt");
var path_file_json = Path.Combine(Environment.CurrentDirectory, "checkpoints_json.txt");
var path_file_xml = Path.Combine(Environment.CurrentDirectory, "checkpoints_xml.txt");

void AjouterCheckpoint(Checkpoint checkpoint, List<Checkpoint> list_chekpoint)
{
    list_chekpoint.Add(checkpoint);
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine($"\nCheckpoint {checkpoint.CheckpointId.ToString()} ajouté.");
    Console.ForegroundColor = ConsoleColor.DarkYellow;
}
#endregion


#region Message_Accueil
afficher($"{monTitre.ToUpper()} ({DateTime.Now.ToString("dd.mm.yyyy")})");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A copy cat of Zelda";
afficher(sousTitre);
Console.ForegroundColor = ConsoleColor.White;
afficher("\nBienvenue\n");
#endregion

while (true)
{
    void AfficherMenuPrincipal()
    {
        string saisieUtilisateur = " ";
        Console.ForegroundColor = ConsoleColor.DarkYellow;
        afficher("\n1. Nouvelle partie\n\n2. Charger Partie\n\n3. Options\n\n4. Quitter\n\n5. entrainement-009\n");
        saisieUtilisateur = Console.ReadLine();
        if (saisieUtilisateur == "1")
        {
            menu.MenuNouvellePartie();
            if (menu.MenuNouvellePartie() == true)
            {
                menu.MenuAjouterHero();
                Hero hero = menu.MenuAjouterHero();
                
            }
            else
            {
                AfficherMenuPrincipal();
            }
        }
        else if (saisieUtilisateur == "2")
        {
            menu.MenuChargerPartie();
        }
        else if (saisieUtilisateur == "3")
        {
            menu.MenuOptions();
        }
        else if (saisieUtilisateur == "4")
        {
            //Quitter
        }
        else if (saisieUtilisateur == "5")
        {

        }

    }

    AfficherMenuPrincipal();
}


