﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game
{
    /// <summary>
    /// Contrat de méthode pour afficher un item
    /// </summary>
    /// <param name="item"></param>
    delegate void Afficher(object item);
}
