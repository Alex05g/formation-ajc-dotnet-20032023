﻿using AutourTVA;
Console.OutputEncoding = System.Text.Encoding.UTF8;

List<double> l_tva = new List<double>();
Afficher afficher = Console.WriteLine;
afficher("\nBienvenue\n");
while (true)
{
    void AfficherMenuPrincipal()
    {
        string saisieUtilisateur = " ";
        Console.ForegroundColor = ConsoleColor.DarkMagenta;
        afficher("\n1. Ajouter une TVA\n\n2. Afficher la liste des TVA\n\n3. Filtrer la liste des TVA\n\n4. Calcul TTC\n");
        saisieUtilisateur = Console.ReadLine();
        if (saisieUtilisateur == "1")
        {
            AjouterDansListeTva(l_tva);
        }
        else if (saisieUtilisateur == "2")
        {
            AfficherListeTva(l_tva);
        }
        else if (saisieUtilisateur == "3")
        {
            FiltrerListeTva(l_tva);
        }
        else if (saisieUtilisateur == "4")
        {
            CalculTTC();
        }

    }

    void AjouterDansListeTva(List<double> liste_tva)
    {
        afficher("Ajoutez la valeur : ");
        string saisieAjout = Console.ReadLine();
        double valeur;
        if (double.TryParse(saisieAjout, out valeur))
        {
            if (saisieAjout != null)
            {
                liste_tva.Add(valeur);
                Console.ForegroundColor = ConsoleColor.Green;
                afficher($"\nAjout de la valeur {saisieAjout} avec succès !\n");
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                afficher("\nErreur : valeur incorrecte");
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
            }
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nValeur incorrecte");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
        }
    };

    void AfficherListeTva(List<double> liste_tva)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        afficher("\nVoici la liste des TVA : \n");
        foreach (var item in liste_tva)
        {
            afficher($"{item} €");
        }
        Console.ForegroundColor = ConsoleColor.White;
    };

    void FiltrerListeTva(List<double> liste_tva)
    {
        var filtrerNumbers = liste_tva.Where(n => n > 1.055);
        Console.ForegroundColor = ConsoleColor.Green;
        afficher("\nVoici les TVA supérieures à 1,055 : \n");
        foreach (var number in filtrerNumbers)
        {
            afficher(number);
        }
        Console.ForegroundColor = ConsoleColor.DarkMagenta;
    }

    void CalculTTC()
    {
        Console.WriteLine("\n Quel est le montant ?\n");
        string saisieMontant = Console.ReadLine();
        double montant;
        if (double.TryParse(saisieMontant, out montant))
        {
            if (saisieMontant != null)
            {
                Console.WriteLine("\nQuelle est la TVA ?\n");
                string saisieTva = Console.ReadLine();
                double tva;
                if (double.TryParse(saisieTva, out tva))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    double result = montant + (montant * tva);
                    Console.WriteLine($"\nRésultat : {result}\n");
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    afficher("\nErreur : valeur incorrecte");
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                afficher("\nErreur : valeur incorrecte");
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
            }
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            afficher("\nErreur : valeur incorrecte");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
        }

    }

    AfficherMenuPrincipal();
}


