﻿using game.api;

namespace starwars.game.Web.UI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class GameListViewModel
    {
        public List<Game> GameList { get; set; } = new();
    }
}
