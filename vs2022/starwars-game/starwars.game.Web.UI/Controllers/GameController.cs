﻿using game.api;
using Microsoft.AspNetCore.Mvc;
using starwars.game.Web.UI.Infrastructures;
using starwars.game.Web.UI.Models;

namespace starwars.game.Web.UI.Controllers
{
	public class GameController : Controller
	{
		public IActionResult Index()
		{
			var sauvegardeur = new BddGameSauvegarde();

			List<Game> games = new()
			{
				new(sauvegardeur) { Id = 1, CheckPointList = new() { new(1, 150) } },
				new(sauvegardeur) { Id = 2, CheckPointList = new() { new(2, 200) } }
			};

			return View(new GameListViewModel()
			{
				GameList = games
			});
		}
	}
}
