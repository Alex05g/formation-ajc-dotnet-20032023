﻿
using Newtonsoft.Json;
using System.Xml.Serialization;
using travail_fichiers;

Console.WriteLine("Contenu ?");
var saisie = Console.ReadLine();

var cheminFichier = Path.Combine(Environment.CurrentDirectory, "test.txt");

// première solution
try
{
    File.WriteAllText(cheminFichier, saisie);
}
catch (PathTooLongException ex)
{

}
catch (IOException ex)
{

}

// deuxième solution

var personne = new Personne()
{
    Id = 1,
    Prenom = "Charlemagne"
};

var type = typeof(Personne);
// type.GetProperties()[0].Name;

XmlSerializer serializer = new XmlSerializer(typeof(Personne));

using var streamF = new FileStream(cheminFichier, FileMode.OpenOrCreate, FileAccess.ReadWrite);
using var stream = new StreamWriter(cheminFichier);
//using var mStream = new MemoryStream
try
{
    serializer.Serialize(stream, personne);
}
finally
{
    stream.Close();
}


using var streamR = new StreamReader(cheminFichier);
personne = serializer.Deserialize(streamF) as Personne;




Console.ReadLine();

string jsonPersonne = JsonConvert.SerializeObject(personne, Formatting.Indented);

try
{
    File.WriteAllText(cheminFichier, jsonPersonne);
}
catch (PathTooLongException ex)
{

}
catch (IOException ex)
{

}
Personne personne2 = JsonConvert.DeserializeObject<Personne>(File.ReadAllText(cheminFichier));


